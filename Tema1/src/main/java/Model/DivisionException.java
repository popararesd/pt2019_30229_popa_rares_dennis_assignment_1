package Model;

@SuppressWarnings("serial")
public class DivisionException extends Exception {
	String error;
	public DivisionException (String e) 
	{
		super(e); 
		error = e;
	} 
	public String getMessage() 
	{
		return error;
	}
}
