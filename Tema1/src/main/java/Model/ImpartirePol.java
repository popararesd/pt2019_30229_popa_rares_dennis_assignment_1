package Model;

import java.util.*;

public class ImpartirePol {
	private Polinom cat;
	private Polinom rest;

	public ImpartirePol(List<Monom> a, List<Monom> b) {
		this.cat = new Polinom(a);
		this.rest = new Polinom(b);
	}

	public ImpartirePol() {
		this.cat = new Polinom();
		this.rest = new Polinom();
	}

	public Polinom getCatP() {
		return this.cat;
	}

	public Polinom getRestP() {
		return this.rest;
	}

	public int hd() {
		return rest.highestDegree();
	}

	public String getRest() {
		return this.rest.toString();
	}

	public String getCat() {
		return this.cat.toString();
	}

	public void sort() {
		this.cat.sort();
		this.rest.sort();
	}

	public void clearPolynomials() {
		this.cat.clearPolynomyal();
		this.rest.clearPolynomyal();
	}
}
