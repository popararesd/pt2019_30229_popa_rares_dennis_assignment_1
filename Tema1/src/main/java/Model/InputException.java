package Model;

@SuppressWarnings("serial")
public class InputException extends Exception { 
	
	private String err; 
	
	public InputException(String e) 
	{
		super(e); 
		err = e;
	} 
	
	public String getMessage() 
	{
		return err;
	}

}
