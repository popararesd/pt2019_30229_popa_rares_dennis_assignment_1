package Model;

@SuppressWarnings("serial")
public class DivideByZeroException extends Exception {
	
	String error;
	
	public DivideByZeroException(String s) 
	{
		super(s); 
	    error = s;
	} 
	
	public String getMessage() 
	{
		return error;
	}
	
}
