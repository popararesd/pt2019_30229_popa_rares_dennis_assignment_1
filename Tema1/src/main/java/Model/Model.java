package Model;

public class Model {
	private Polinom op1;
	private Polinom op2;
	private Polinom rez;
	private ImpartirePol imp;

	public Model() {
		this.op1 = new Polinom();
		this.op2 = new Polinom();
		this.rez = new Polinom();
		this.imp = new ImpartirePol();
	}

	public void impartire(String opr1, String opr2) throws Exception {
		this.op1.StringToPolinoame(opr1);
		this.op2.StringToPolinoame(opr2);
		this.imp = this.op1.impartire(this.op2);
		this.imp.sort();
	}

	public String getCat() {
		return this.imp.getCat();
	}

	public String getRest() {
		return this.imp.getRest();
	}

	public String inmultire(String opr1, String opr2) throws Exception {
		this.op1.StringToPolinoame(opr1);
		this.op2.StringToPolinoame(opr2);
		this.rez = this.op1.inmultire(this.op2);
		rez.sort();
		return this.rez.toString();
	}

	public String adunare(String opr1, String opr2) throws Exception {
		this.op1.StringToPolinoame(opr1);
		this.op2.StringToPolinoame(opr2);
		rez = this.op1.adunare(this.op2);
		rez.sort();
		return rez.toString();
	}

	public String scadere(String opr1, String opr2) throws Exception {
		this.op1.StringToPolinoame(opr1);
		this.op2.StringToPolinoame(opr2);
		this.rez = this.op1.scadere(this.op2);
		this.rez.sort();
		return this.rez.toString();
	}

	public String derivare(String opr) throws Exception {
		this.op1.StringToPolinoame(opr);
		this.rez = this.op1.derivare();
		rez.sort();
		return this.rez.toString();
	}

	public String integrare(String opr) throws Exception {
		this.op1.StringToPolinoame(opr);
		this.rez = this.op1.integrare();
		rez.sort();
		return this.rez.toString();
	}

	public void resetPolinomyals() {
		this.op1.clearPolynomyal();
		this.op2.clearPolynomyal();
		this.rez.clearPolynomyal();
		this.imp.clearPolynomials();
	}
}
