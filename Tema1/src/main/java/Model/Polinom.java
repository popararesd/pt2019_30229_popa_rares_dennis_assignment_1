package Model;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom extends Monom {

	private ArrayList<Monom> poli;

	public Polinom() {
		super();

		this.poli = new ArrayList<Monom>();
	}

	public Polinom(List<Monom> a) {
		this.poli = new ArrayList<Monom>(a);
	}

	public int getSize() {
		return this.poli.size();
	}

	public ArrayList<Monom> getList() {
		return this.poli;
	}

	private void DegreeGTZ(String s, String ptrn) {
		String pattern = ptrn;
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(s);

		while (m.find()) {
			String coef = "", gr = "";
			coef = m.group(1) == null ? "+" : m.group(1);
			coef += m.group(2) == null ? "1" : m.group(2);
			gr = m.group(4) == null ? "+" : m.group(4);
			gr += m.group(5) == null ? "1" : m.group(5);
			if (m.group(3) == null)
				gr = "0";
			System.out.println();

			this.poli.add(new Monom(Integer.parseInt(gr), Integer.parseInt(coef)));

		}
	}

	private void DegreeOne(String s, String ptrn) {

		String pattern = ptrn;
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(s);
		while (m.find()) {
			String st = m.group(1);
			st = st.substring(0, st.length() - 1);
			st += (st.equals("-") || st.equals("+")) ? "1" : "";

			this.poli.add(new Monom(1, Integer.parseInt(st)));

		}

	}

	private void DegreeZero(String s, String ptrn) {
		String pattern = ptrn;
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(s);
		while (m.find()) {
			this.poli.add(new Monom(0, Integer.parseInt(m.group(1))));
		}
	}

	public void StringToPolinoame(String s) throws InputException {

		try {
			String pattern1 = "(-|\\+)?(\\d+)?([a-z])\\^(-|\\+)?(\\d+)";
			String pattern2 = "(((\\+?)(-?)(\\d?)[a-z]))";
			String pattern3 = "(-?\\d+)";
			s = s.replaceAll("\\s+", "");
			if (s.equals("x") || s.equals("X") ) {
				this.poli.add(new Monom(1, 1));
			} else {
				this.DegreeGTZ(s, pattern1);
				s = s.replaceAll(pattern1, "");
				this.DegreeOne(s, pattern2);
				s = s.replaceAll(pattern2, "");
				this.DegreeZero(s, pattern3);
				s = s.replace(pattern3, "");
			}
		}

		catch (Exception e) {
			throw new InputException("Inputul este invalid!");
		}
	}

	@Override
	public String toString() {
		String rez = "";
		for (Monom m : this.poli)
			rez += m.toString();
		return rez;
	}

	public void levelPoli() {
		for (int i = 0; i < this.poli.size(); i++)
			for (int j = i + 1; j < this.poli.size(); j++) {
				if (this.poli.get(i).getGrad() == this.poli.get(j).getGrad()) {
					double c1 = this.poli.get(i).getCoeficient(), c2 = this.poli.get(j).getCoeficient();
					this.poli.get(i).setCoeficient(c1 + c2);
					this.poli.remove(j);
					j--;
				}

			}
	}

	public Polinom adunare(Polinom p) throws Exception {
		try {
			Polinom rez = new Polinom();
			List<Monom> list1 = new ArrayList<Monom>(this.poli);
			List<Monom> list2 = new ArrayList<Monom>(p.poli);

			for (int i = 0; i < list1.size(); i++) {
				for (int j = 0; j < list2.size() && list1.size() > 0; j++) {
					if (list1.get(i).getGrad() == list2.get(j).getGrad()) {
						rez.poli.add(list1.get(i).adunare(list2.get(j)));
						list2.remove(j);
						list1.remove(i);
						j--;
					}

				}
			}
			for (Monom a : list1)
				rez.poli.add(a);
			for (Monom b : list2)
				rez.poli.add(b);

			return rez;
		} catch (Exception e) {
			throw new Exception("Eroare neprevazuta!");
		}
	}

	public Polinom scadere(Polinom p) throws Exception {

		try {
			Polinom rez = new Polinom();
			List<Monom> list1 = new ArrayList<Monom>(this.poli);
			List<Monom> list2 = new ArrayList<Monom>(p.poli);

			for (int i = 0; i < list1.size(); i++) {
				for (int j = 0; j < list2.size() && list1.size() > 0; j++) {
					if (list1.get(i).getGrad() == list2.get(j).getGrad() && (!list1.isEmpty() && !list2.isEmpty())) {
						rez.poli.add(list1.get(i).scadere(list2.get(j)));
						list2.remove(j);
						list1.remove(i);
						j--;
					}

				}
			}
			for (Monom a : list1)
				rez.poli.add(a);
			for (Monom b : list2)
				rez.poli.add(new Monom(b.getGrad(), -b.getCoeficient()));

			return rez;
		} catch (Exception e) {
			throw new Exception("Eroare neprevazuta!");
		}

	}

	public Polinom derivare() throws Exception {
		try {
			Polinom rez = new Polinom();
			for (Monom a : this.poli)
				rez.poli.add(a.derivare());
			return rez;
		} catch (Exception e) {
			throw new Exception("Eroare neprevazuta!");
		}
	}

	public Polinom integrare() throws Exception {
		try {
			Polinom rez = new Polinom();
			for (Monom a : this.poli)
				rez.poli.add(a.integrare());
			return rez;
		} catch (Exception e) {
			throw new Exception("Eroare neprevazuta!");
		}
	}

	public Polinom inmultire(Polinom p) throws Exception {
		try {
			Polinom rez = new Polinom();
			for (Monom a : this.poli)
				for (Monom b : p.poli)
					rez.poli.add(a.inmultire(b));
			rez.levelPoli();
			return rez;
		} catch (Exception e) {
			throw new Exception("Eroare neprevazuta!");
		}
	}

	public void removeZeros() {
		for (int i = 0; i < this.poli.size(); i++) {
			if (this.poli.get(i).getCoeficient() == 0) {
				this.poli.remove(i);
				i--;
			}
		}
	}

	public ImpartirePol impartire(Polinom d) throws Exception {
		Polinom cat = new Polinom();
		Polinom rest = new Polinom();
		Collections.sort(this.poli);
		Collections.sort(d.poli);

		if (d.poli.get(0).compareTo(new Monom(0, 0)) == 0 && d.poli.get(0).getCoeficient() == 0)
			throw new DivideByZeroException("Impartire invalida.Numitorul nu poate fi 0");
		if (d.highestDegree() > this.highestDegree())
			throw new DivisionException("Gradul deimpartitului este mai mic decat al impartitorului");

		cat.poli.add(new Monom(0, 0));
		rest.poli = new ArrayList<Monom>(this.poli);

		while ((!rest.poli.isEmpty()) && (rest.highestDegree() >= d.highestDegree())) {
			Monom aux = rest.poli.get(0).impartire(d.poli.get(0));
			Polinom p = new Polinom();
			p.poli.add(aux);
			try {
				cat = cat.adunare(p);
				rest = rest.scadere(p.inmultire(d));
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}
			cat.removeZeros();
			rest.removeZeros();
			Collections.sort(cat.poli);
			Collections.sort(rest.poli);
			p.clearPolynomyal();

		}

		ImpartirePol output = new ImpartirePol(cat.poli, rest.poli);

		return output;

	}

	public int highestDegree() {
		Collections.sort(this.poli);
		Monom aux = this.poli.get(0);
		if (aux != null)
			return aux.getGrad();
		else
			return 0;
	}

	public void sort() {
		Collections.sort(this.poli);
	}

	public void clearPolynomyal() {
		this.poli.removeAll(poli);
	}

	public static void main(String[] args) throws Exception {
		String s1 = "x";
		String s2 = "x";
		Polinom p = new Polinom();
		Polinom q = new Polinom();
		try {
			p.StringToPolinoame(s1);
		} catch (InputException e) {
			System.out.println(e.getMessage());
		}
		q.StringToPolinoame(s2);
		System.out.println(p);
		System.out.println(q);
		ImpartirePol rez = p.impartire(q);
		System.out.println("Cat " + rez.getCat());
		System.out.println("Rest : " + rez.getRest());
	}

}
