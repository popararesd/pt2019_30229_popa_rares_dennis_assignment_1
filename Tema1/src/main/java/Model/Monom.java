package Model;

public class Monom implements Comparable<Monom> {

	private int grad;
	private double coeficient;

	public Monom(int grad, double coeficient) {
		super();
		this.grad = grad;
		this.coeficient = coeficient;
	}

	public Monom() {
		this.grad = 0;
		this.coeficient = 0;
	}

	public int getGrad() {
		return grad;
	}

	public void setGrad(int grad) {
		this.grad = grad;
	}

	public double getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}

	public Monom adunare(Monom m) {

		return new Monom(this.grad, this.coeficient + m.getCoeficient());

	}

	public Monom scadere(Monom m) {

		return new Monom(this.grad, this.coeficient - m.getCoeficient());

	}

	public Monom inmultire(Monom m) {
		return new Monom(this.grad + m.getGrad(), this.coeficient * m.getCoeficient());
	}

	public Monom impartire(Monom m) {
		return new Monom(this.grad - m.getGrad(), this.coeficient / m.getCoeficient());
	}

	public Monom derivare() throws Exception {
		int g = this.getGrad();
		double c = this.getCoeficient();
		if (g == 0)
			return new Monom(0, 0);
		return new Monom(g - 1, g * c);
	}

	public Monom integrare() throws Exception {
		int g = this.getGrad();
		double c = this.getCoeficient();
		return new Monom(g + 1, c / (g + 1));
	}

	public int compareTo(Monom o) {
		// TODO Auto-generated method stub
		return -Integer.compare(this.getGrad(), o.getGrad());
	}

	public String toString() {
		if (this.getCoeficient() == 0)
			return "+0";
		String rez = "";
		if (this.getCoeficient() > 0)
			rez = "+";

		if (this.getGrad() >= 2) {
			if (this.getCoeficient() == Math.floor(this.getCoeficient()))
				return rez + (int) this.getCoeficient() + "x^" + this.getGrad();
			else
				return rez + String.format("%.2f", this.getCoeficient()) + "x^" + this.getGrad();
		} else {
			if (this.getGrad() == 1)
				if (this.getCoeficient() == Math.floor(this.getCoeficient()))
					return rez + (int) this.getCoeficient() + "x";
				else
					return rez + String.format("%.2f", this.getCoeficient()) + "x";
			else if (this.getCoeficient() == Math.floor(this.getCoeficient()))
				return rez + this.getCoeficient();
			else
				return rez + String.format("%.2f", this.getCoeficient());

		}

	}

}
