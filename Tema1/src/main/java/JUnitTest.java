import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;

import Model.*;

public class JUnitTest {

	
	@Test 
	public void adunareTest() throws Exception 
	{ 
		List <Monom> l1 = new ArrayList <Monom>();  
		List <Monom> l2 = new ArrayList <Monom>(); 
		List <Monom> l3 = new ArrayList <Monom>();
		l1.add(new Monom(2,2)); 
		l1.add(new Monom(1,-1)); 
		l1.add(new Monom(0,7)); 
		l2.add(new Monom(2,3)); 
		l2.add(new Monom(1,5)); 
		l2.add(new Monom(0,-3)); 
		l3.add(new Monom(2,5)); 
		l3.add(new Monom(1,4)); 
		l3.add(new Monom(0,4));
		Polinom p = new Polinom(l1); 
		Polinom q = new Polinom(l2); 
		Polinom rez = p.adunare(q);  
		Polinom rez1 = new Polinom(l3); 
		
		rez.sort(); 
		rez1.sort(); 
		
		assertEquals(rez.getSize(),rez1.getSize()); 
		
		for(int i =0 ;i<rez.getSize();i++) 
		{
			assertEquals((int)rez.getList().get(i).getCoeficient(), (int)rez1.getList().get(i).getCoeficient()); 
			assertEquals(rez.getList().get(i).getGrad(), rez1.getList().get(i).getGrad());
		}
		
	} 
	
	@Test 
	public void scadereTest() throws Exception 
	{ 
		List <Monom> l1 = new ArrayList <Monom>();  
		List <Monom> l2 = new ArrayList <Monom>(); 
		List <Monom> l3 = new ArrayList <Monom>();
		l1.add(new Monom(2,2)); 
		l1.add(new Monom(1,-1)); 
		l1.add(new Monom(0,7)); 
		l2.add(new Monom(2,3)); 
		l2.add(new Monom(1,5)); 
		l2.add(new Monom(0,-3)); 
		l3.add(new Monom(2,-1)); 
		l3.add(new Monom(1,-6)); 
		l3.add(new Monom(0,10));
		Polinom p = new Polinom(l1); 
		Polinom q = new Polinom(l2); 
		Polinom rez = p.scadere(q);  
		Polinom rez1 = new Polinom(l3); 
		
		rez.sort(); 
		rez1.sort(); 
		
		assertEquals(rez.getSize(),rez1.getSize()); 
		
		for(int i =0 ;i<rez.getSize();i++) 
		{
			assertEquals((int)rez.getList().get(i).getCoeficient(), (int)rez1.getList().get(i).getCoeficient()); 
			assertEquals(rez.getList().get(i).getGrad(), rez1.getList().get(i).getGrad());
		}
		
	} 
	

	@Test 
	public void derivareTest() throws Exception 
	{ 
		List <Monom> l1 = new ArrayList <Monom>();  
		List <Monom> l2 = new ArrayList <Monom>(); 
		
		l1.add(new Monom(2,2)); 
		l1.add(new Monom(1,-1)); 
		l1.add(new Monom(0,7)); 
		l2.add(new Monom(1,4)); 
		l2.add(new Monom(0,-1)); 
		l2.add(new Monom(0,0)); 
		
		Polinom p = new Polinom(l1); 
		Polinom q = new Polinom(l2); 
		Polinom rez = p.derivare();  
		 
		
		rez.sort(); 
		q.sort(); 
		
		assertEquals(rez.getSize(),q.getSize()); 
		
		for(int i =0 ;i<rez.getSize();i++) 
		{
			assertEquals((int)rez.getList().get(i).getCoeficient(), (int)q.getList().get(i).getCoeficient()); 
			assertEquals(rez.getList().get(i).getGrad(), q.getList().get(i).getGrad());
		}
		
	} 
	
	@Test 
	public void integrareTest() throws Exception 
	{ 
		List <Monom> l1 = new ArrayList <Monom>();  
		List <Monom> l2 = new ArrayList <Monom>(); 
		
		l1.add(new Monom(2,2)); 
		l1.add(new Monom(1,-1)); 
		l1.add(new Monom(0,7)); 
		l2.add(new Monom(3,3/4)); 
		l2.add(new Monom(2,-1/2)); 
		l2.add(new Monom(1,7/1)); 
		
		Polinom p = new Polinom(l1); 
		Polinom q = new Polinom(l2); 
		Polinom rez = p.integrare();  
		 
		
		rez.sort(); 
		q.sort(); 
		
		assertEquals(rez.getSize(),q.getSize()); 
		
		for(int i =0 ;i<rez.getSize();i++) 
		{
			assertEquals((int)rez.getList().get(i).getCoeficient(), (int)q.getList().get(i).getCoeficient()); 
			assertEquals(rez.getList().get(i).getGrad(), q.getList().get(i).getGrad());
		}
		
	} 
	
	@Test 
	public void inmultireTest() throws Exception 
	{ 
		List <Monom> l1 = new ArrayList <Monom>();  
		List <Monom> l2 = new ArrayList <Monom>(); 
		List <Monom> l3 = new ArrayList <Monom>();
		l1.add(new Monom(2,2)); 
		l1.add(new Monom(1,-1)); 
		l1.add(new Monom(0,7)); 
		l2.add(new Monom(2,3)); 
		l2.add(new Monom(1,5)); 
		l2.add(new Monom(0,-3)); 
		l3.add(new Monom(4,6)); 
		l3.add(new Monom(3,7)); 
		l3.add(new Monom(2,10)); 
		l3.add(new Monom(1,38)); 
		l3.add(new Monom(0,-21));
		Polinom p = new Polinom(l1); 
		Polinom q = new Polinom(l2); 
		Polinom rez = p.inmultire(q);  
		Polinom rez1 = new Polinom(l3); 
		
		rez.sort(); 
		rez1.sort(); 
		
		assertEquals(rez.getSize(),rez1.getSize()); 
		
		for(int i =0 ;i<rez.getSize();i++) 
		{
			assertEquals((int)rez.getList().get(i).getCoeficient(), (int)rez1.getList().get(i).getCoeficient()); 
			assertEquals(rez.getList().get(i).getGrad(), rez1.getList().get(i).getGrad());
		}
		
	}  
	
	public void impartireTest() throws Exception 
	{ 
		List <Monom> l1 = new ArrayList <Monom>();  
		List <Monom> l2 = new ArrayList <Monom>(); 
		List <Monom> l3 = new ArrayList <Monom>();
		List <Monom> l4 = new ArrayList <Monom>();
		
		l1.add(new Monom(2,2)); 
		l1.add(new Monom(1,-1)); 
		l1.add(new Monom(0,7)); 
		l2.add(new Monom(2,3)); 
		l2.add(new Monom(1,5)); 
		l2.add(new Monom(0,-3)); 
		
		l3.add(new Monom(0,2/3)); 
		l4.add(new Monom(1,-4)); 
		l4.add(new Monom(0,9)); 
		
		Polinom p = new Polinom(l1); 
		Polinom q = new Polinom(l2); 
		ImpartirePol result = p.impartire(q); 
		result.sort();
		
		Polinom rez = result.getCatP();
		Polinom rez1 = new Polinom (l3);
		assertEquals(rez.getSize(),rez1.getSize()); 
		
		for(int i =0 ;i<rez.getSize();i++) 
		{
			assertEquals((int)rez.getList().get(i).getCoeficient(), (int)rez1.getList().get(i).getCoeficient()); 
			assertEquals(rez.getList().get(i).getGrad(), rez1.getList().get(i).getGrad());
		} 
		
		rez = result.getRestP();
		rez1 = new Polinom (l4);
		assertEquals(rez.getSize(),rez1.getSize()); 
		
		for(int i =0 ;i<rez.getSize();i++) 
		{
			assertEquals((int)rez.getList().get(i).getCoeficient(), (int)rez1.getList().get(i).getCoeficient()); 
			assertEquals(rez.getList().get(i).getGrad(), rez1.getList().get(i).getGrad());
		}
		
	} 
	
}
