package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Model.*;
import View.*;

public class Controller {

	private Model model;
	private ViewGui view;

	public Controller(Model model, ViewGui view) {
		this.model = model;
		this.view = view;
		view.addExecuteListener(new OperationListener());
		view.addSelectionListener(new SelectionListener());

	}

	class SelectionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.reset();
			view.updateCurrentState();
			model.resetPolinomyals();
			switch (view.getCurrentStateCmbBox()) {
			case 0:
				view.setDivision(false);
				view.set2OperandsVisible(true);
				break;
			case 1:
				view.setDivision(false);
				view.set2OperandsVisible(true);
				break;
			case 2:
				view.setDivision(false);
				view.set2OperandsVisible(true);
				break;
			case 3:
				view.setDivision(true);
				view.set2OperandsVisible(true);
				break;
			case 4:
				view.setDivision(false);
				view.set2OperandsVisible(false);
				break;
			case 5:
				view.setDivision(false);
				view.set2OperandsVisible(false);
				break;
			default:
				view.setDivision(false);
				view.set2OperandsVisible(true);

			}

		}
	}

	class OperationListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			model.resetPolinomyals();
			try {

				switch (view.getCurrentStateCmbBox()) {
				case 0:
					view.setResult(model.adunare(view.getFirstPoli(), view.getSecondPoli()));
					break;
				case 1:
					view.setResult(model.scadere(view.getFirstPoli(), view.getSecondPoli()));
					break;
				case 2:
					view.setResult(model.inmultire(view.getFirstPoli(), view.getSecondPoli()));
					break;
				case 3:
					model.impartire(view.getFirstPoli(), view.getSecondPoli());
					view.setResult(model.getCat());
					view.setRest(model.getRest() == "" ? "0" : model.getRest());
					break;
				case 4:
					view.setResult(model.derivare(view.getFirstPoli()));
					break;
				case 5:
					view.setResult(model.integrare(view.getFirstPoli()));
					break;

				}

			} catch (InputException er) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, er.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} catch (DivideByZeroException er2) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, er2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} catch (DivisionException er3) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, er3.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} catch (Exception er4) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, er4.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}

		}

	}

}
