package View;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class ViewGui extends JFrame {

	private JTextField tfPoli1 = new JTextField(30);
	private JTextField tfPoli2 = new JTextField(30);
	private JTextField tfRez = new JTextField(30);
	private JButton executeBtn = new JButton("Executa");
	private JComboBox<String> actions;
	private JLabel pol2 = new JLabel("Polinomul 2:");
	private JTextField tfRest = new JTextField(30);
	private int state;

	public ViewGui() {
		tfRez.setText("0");
		tfRez.setEditable(false);
		tfRest.setEditable(false);
		String[] operatii = { "Adunare", "Scadere", "Inmultire", "Impartire", "Derivare", "Integrare" };
		actions = new JComboBox<String>(operatii);
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(5, 2));
		panel.add(new JLabel("Polinomul 1:"));
		panel.add(new JLabel("Alegeti Operatia"));
		panel.add(tfPoli1);
		panel.add(actions);
		panel.add(pol2);
		pol2.setVisible(false);
		panel.add(new JLabel("Rezultat"));
		panel.add(tfPoli2);
		tfPoli2.setVisible(false);
		panel.add(tfRez);
		panel.add(executeBtn);
		panel.add(tfRest);
		tfRest.setVisible(false);
		this.setContentPane(panel);
		this.pack();
		this.setTitle("Sistem procesare polinoame");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.state = actions.getSelectedIndex();

	}

	public void updateCurrentState() {
		this.state = actions.getSelectedIndex();
	}

	public int getCurrentStateCmbBox() {
		return this.state;
	}

	public void reset() {
		tfRez.setText("0");
		tfRest.setText("0");
		tfPoli1.setText("");
		tfPoli2.setText("");
	}

	public String getFirstPoli() {
		return tfPoli1.getText();
	}

	public String getSecondPoli() {
		return tfPoli2.getText();
	}

	public void setResult(String result) {
		tfRez.setText(result);
	}

	public void setRest(String rest) {
		tfRest.setText(rest);
	}

	public void set2OperandsVisible(boolean bool) {
		tfPoli2.setVisible(bool);
		pol2.setVisible(bool);
	}

	public void setDivision(boolean bool) {
		tfRest.setVisible(bool);
	}

	public void addExecuteListener(ActionListener exe) {
		executeBtn.addActionListener(exe);
	}

	public void addSelectionListener(ActionListener sel) {
		actions.addActionListener(sel);
	}

	public void initialize() {
		this.set2OperandsVisible(true);
		this.setVisible(true);
	}

}
