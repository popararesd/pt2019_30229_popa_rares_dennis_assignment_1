package PT2019.assingment1.Tema1;

import Controller.Controller;
import Model.Model;
import View.ViewGui;

/**
 * MVC - Sistem procesare polinoame
 *
 */
public class App {
	public static void main(String args[]) {
		Model model = new Model();
		ViewGui view = new ViewGui();
		@SuppressWarnings("unused")
		Controller controller = new Controller(model, view);
		view.initialize();
	}
}
